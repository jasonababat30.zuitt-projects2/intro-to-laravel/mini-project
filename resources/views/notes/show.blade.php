@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h1>{{ $note->title }} <small class="badge badge-pill badge-secondary">{{ $note->is_active? '' : 'archived' }}</small> </h1>
        </div>
        <div class="col-md-8">
            <div class="row">
              <div class="card mb-3" style="width:100%;">
                <div class="card-body">
                  <h3 class="card-title">
                    <a href="#"><small>Posted by: {{ $note->user->name }}</small></a>
                  </h3>
                  <p class="card-text">{{ $note->content }}</p>
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6">
                        <a class="btn btn-link" href="/notes#{{ $note->id }}">Back to Notes List</a>
                      </div>
                      <div class="col-md-6">
                        @if(Auth::check())
                        <div class="row justify-content-end">
                          <a href="/notes/{{ $note->id }}/edit" class="btn btn-primary mr-1">Edit</a>
                          <form method="POST" action="/notes/{{ $note->id }}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="btn btn-danger mr-1">Delete</button>
                          </form>
                          <form method="POST" action="/notes/{{ $note->id }}/archive">
                            @method('PUT')
                            @csrf
                            <button type="submit" class="btn btn-warning">Archive</button>
                          </form>
                        </div>
                        @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Note;
use Auth;

class NoteController extends Controller
{
	public function __construct()

	{
		$this->middleware('auth')->except(['index', 'show']);
	}

	// Create a new Note
	// Endpoint: GET /notes/create
    public function create()

    {
    	return view('notes.create');
    }

    // Endpoint: POST /notes
    public function store(Request $req)

    {
    	// Create a new note object

    	$new_note = new Note([
    		'title' => $req->input('title'),

    		'content' => $req->input('content'),

    		'user_id' => Auth::user()->id
    	]);

    	// Save to DB

    	$new_note->save();

    	// Redirect

    	return redirect('/notes/create');
    }

    // Endpoint: GET /notes
    public function index()

    {
    	$notes_list = Note::all();
    	return view('notes.index')->with('notes', $notes_list);
    }

    // Endpoint: GET /notes/{note_id}
    public function show($note_id)

    {
    	// Retrieve a specific note
    	$note = Note::find($note_id);
    	return view('notes.show')->with('note', $note);
    }

    // Endpoint: GET /notes/my-notes
    public function myNotes()

    {
    	$my_notes = Auth::user()->notes;

    	return view('notes.index')->with('notes', $my_notes);
    }

    // Endpoint: GET /notes/{note_id}/edit
    public function edit($note_id)

    {
    	// Find the note to be updated
    	$existing_note = Note::find($note_id);

    	// Redirect the user to page where to note will be updated
    	return view('notes.edit')->with('note', $existing_note);
    }

    // Endpoint: PUT /notes/{note_id}
    public function update($note_id, Request $req)

    {
    	// Find an existing note to be updated
    	$existing_note = Note::find($note_id);

    	// Set the new values of an existing note
    	$existing_note->title = $req->input('title');
    	
    	$existing_note->content = $req->input('content');
    	
    	$existing_note->save();

    	// Redirect the user to the page of that individual note
    	return redirect("/notes/$note_id");
    }

    // Endpoint: DELETE /notes/{note_id}
    public function destroy($note_id)

    {
    	// Find the existing note to be deleted
    	$existing_note = Note::find($note_id);

    	// Delete the note
    	$existing_note->delete();

    	// Redirect
    	return redirect('/notes');
    }

    // Endpoint: /notes/{note_id}/archive
    public function archive($note_id)

    {
    	// Find an existing note to be archived
    	$existing_note = Note::find($note_id);

    	// Set the new value of the is_active field
    	$existing_note->is_active = false;

    	$existing_note->save();

    	// Redirect

    	return redirect("/notes/$note_id");
    }

    // Endpoint: /notes/archived
    public function archivedNotes()

    {
    	$archived_notes = Auth::user()->notes;

    	return view('notes.archived')->with('archived_notes', $archived_notes);
    }
}
